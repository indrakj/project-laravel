<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resep extends Model
{
    protected $table = 'resep';

    protected $fillable = ['judul', 'content', 'thumbnail', 'kategori_id'];

    public function komentar()
    {
        return $this->hasMany('App\Komentar');
    }
}
